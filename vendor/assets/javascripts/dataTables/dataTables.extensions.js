var validChars = "$£€c" + "0123456789" + " .-,'";
 
// Init the regex just once for speed - it is "closure locked"
var
    str = jQuery.fn.dataTableExt.oApi._fnEscapeRegex( validChars ),
    re = new RegExp('[^'+str+']');
 
 
jQuery.fn.dataTableExt.aTypes.unshift(
   function ( data )
    {
        if ( typeof data !== 'string' || re.test(data) ) {
            return null;
        }
        return 'currency';
    }
);
 
jQuery.fn.dataTableExt.aTypes.unshift(
    function ( sData )
    {
        var sValidChars = "0123456789,.";
        var Char;
        var bDecimal = false;
        var iStart=0;

        /* Negative sign is valid - shift the number check start point */
        if ( sData.charAt(0) === '-' ) {
            iStart = 1;
        }
 
        /* Check the numeric part */
        for ( var i=iStart ; i<sData.length ; i++ )
        {
            Char = sData.charAt(i);
            if (sValidChars.indexOf(Char) == -1)
            {
                return null;
            }
        }
 
        return 'numeric-comma';
    }
);
jQuery.extend( jQuery.fn.dataTableExt.oSort, {
    "currency-pre": function ( a ) {
        a = (a==="-") ? 0 : a.replace( /[^\d\-\.]/g, "" );
        return parseFloat( a );
    },
 
    "currency-asc": function ( a, b ) {
        return a - b;
    },
 
    "currency-desc": function ( a, b ) {
        return b - a;
    }
} );