module Inspinia
	class Engine < ::Rails::Engine 
    initializer 'inspinia.load_static_assets' do |app|
      app.middleware.use ::ActionDispatch::Static, "#{root}/vendor/images"
      app.middleware.use ::ActionDispatch::Static, "#{root}/vendor/javascripts"
      app.middleware.use ::ActionDispatch::Static, "#{root}/vendor/stylesheets"
    end

		config.to_prepare do
#			config.assets.precompile += %w(*.png *.jpg *.jpeg *.gif)
			Rails.application.config.assets.precompile += %w( *.png *.jpg *.jpeg *.gif empty.css.scss inspinia_gem.css.scss inspinia_template.js inspinia_gem.js inspinia_gem2.js)
		end
	end
end