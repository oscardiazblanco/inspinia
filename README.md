# Inspinia


## Installation

Add this line to your application's Gemfile:

```ruby
gem 'inspinia'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install inspinia

## Usage

add devise gem
rails generate devise:intstall
config.action_mailer.default_url_options = { host: 'localhost', port: 3000 }
rails generate devise User
rake db:migrate

Add this to application controller to select devise template:

layout :layout_by_resource
before_action :authenticate_user!

private

def layout_by_resource
  if devise_controller?
    "inspinia_devise"
  else
    "inspinia"
  end
end


Define:

@title -> Page title (String)
@footer_right_text -> Admin footer right text (String)
@footer_left_text -> Admin footer left text (String)

@menu -> Left Menu. Array of hashes: [{title: String, icon:string(i container, or image container), link:string || submenus:[{title:String, icon:string(i container, or image container), link:String}]}]

@avatar_url -> url for the user image (string)
@user_full_name -> full name of current user (String)
@user_position -> position in company (string)
@user_menu -> Menu that appears on the user - Array of hashes [{title: String, link: String}] if link is "divider" it inserts a divider
@small_logo -> URL of the image that appears when the menu is collapsed (string)
@logout_url -> URL for session close (string)
@logout_title -> Menu title for logout (string)

# Partials
## Table Partial
table_partial: data is a hash object with the following keys: 
	- title, string
	- subtitle, string
	- headers, array of strings
	- column_classes, array of strings
	- column_widths, array of integers
	- rows, array of array of strings


