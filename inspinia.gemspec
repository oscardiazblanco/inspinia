# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'inspinia/version'

Gem::Specification.new do |spec|
  spec.name          = "inspinia"
  spec.version       = Inspinia::VERSION
  spec.authors       = ["Oscar Diaz"]
  spec.email         = ["oscar@zinetika.com"]

  spec.summary       = %q{Inspinia Admin Template}
  spec.homepage      = "http://zinetika.com"
  spec.license       = "MIT"

  # # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # # to allow pushing to a single host or delete this section to allow pushing to any host.
  # if spec.respond_to?(:metadata)
  #   spec.metadata['allowed_push_host'] = "http://git.zinetika.com'"
  # else
  #   raise "RubyGems 2.0 or newer is required to protect against " \
  #     "public gem pushes."
  # end

  spec.files         = `git ls-files`.split("\n")
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.14"
  spec.add_development_dependency "rake", "~> 10.0"

  spec.add_dependency "jquery-rails", '~> 4.3.5'

	spec.add_dependency 'font-awesome-sass', '~> 5.6.1'

end
