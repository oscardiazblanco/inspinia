module InspiniaHelper

  def is_active_controller(item, class_name="active")
  	current_controller = controller_path
  	route_controller_action = Rails.application.routes.recognize_path(item[:link])
  	item_controllers = []
  	item_controllers << route_controller_action[:controller] unless route_controller_action[:controller].blank?
  	unless item[:submenus].blank?
	  	item[:submenus].each do |submenu|
		  	item_controllers << Rails.application.routes.recognize_path(submenu[:link])[:controller]
	  	end
	  end

  	if item_controllers.include?current_controller
  		return class_name
  	else
  		return nil
  	end
  end




  def is_active_action(item)
	  	route_controller_action = Rails.application.routes.recognize_path(item[:link])
      (params[:controller]==route_controller_action[:controller] && params[:action]==route_controller_action[:action]) ? "active" : nil
  end

  def alignment_class(alignment)
    case alignment
    when "C"
      a="text-center"
    when "L"
      a="text-left"
    when "R"
      a="text-right"
    else
      a="text-left"
    end
    return a
  end
  def cell_format(value, format)
    #S: STRING
    #C: CURRENCY.DECIMALS.CURRENCY_CODE
    #P: PERCENT.DECIMALS
    #D: DATE.i18n_format
    formatted_value = ""
    unless value.blank?
      case format
      when "S"
        formatted_value = value.to_s
      when /^D.*/
        format_from_formatter = format.split(".")[1].to_sym
        formatted_value = I18n.l(value, format: format_from_formatter)
      when /^C.*/
        decimals = format.split(".")[1].to_i
#       formatter = (I18n.locale=="es" ? '\&.' : '\&,')
#       formatted_value =  ((value||0)+0.5).to_i.to_s.reverse.gsub(/...(?=.)/,formatter).reverse
        formatted_value = number_to_currency(value, precision: decimals, unit: format.split(".")[2] )
      when /^N.*/
        decimals = format.split(".")[1].to_i
        formatted_value = number_with_precision(value, precision:decimals)
      when /^P.*/
        decimals = format.split(".")[1].to_i
        formatted_value = "#{number_with_precision(value, precision:decimals)}%"
      when "M"
        formatted_value = link_to value, "mailto:#{value}"
      end
    end
    return formatted_value
  end

  def link_to_add_fields(f, association, options={})
    partial_name = association.to_s.singularize + "_fields"
    partial_name = options[:partial_name] unless options[:partial_name].blank?
    link_id = association.to_s
    link_id = options[:link_id] unless options[:link_id].blank?
    destination_div = association.to_s
    destination_div = options[:destination_div] unless options[:destination_div].blank?

    new_object = f.object.class.reflect_on_association(association).klass.new
    fields = f.fields_for(association, new_object, :child_index => "new_#{destination_div}") do |builder|
      unless options[:form_locals].blank?
        form_locals=options[:form_locals]
        form_locals[:f] = builder
      else
        form_locals = {}
        form_locals[:f] = builder
      end
      render(partial_name, form_locals)
    end
    raw("$('a##{link_id}').click(function(event) {event.preventDefault(); add_fields(\"#{destination_div}\", \"#{escape_javascript(fields)}\")});")
  end


end
